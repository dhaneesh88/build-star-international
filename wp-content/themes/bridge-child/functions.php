<?php

// enqueue the child theme stylesheet

Function qode_child_theme_enqueue_scripts() {
	wp_register_style( 'childstyle', get_stylesheet_directory_uri() . '/style.css'  );
	wp_enqueue_style( 'childstyle' );
}
add_action( 'wp_enqueue_scripts', 'qode_child_theme_enqueue_scripts', 11);
function create_posttype() {
	register_post_type( 'project_details',
	// CPT Options
	array(
	'labels' => array(
	'name' => __( 'Project Details' ),
	'singular_name' => __( 'Project detail' )
	),
	'public' => true,
	'has_archive' => true,
	'rewrite' => array('slug' => 'project-details'),
	)
	);
	register_taxonomy_for_object_type( 'category', 'project_details' );
}
// Hooking up our function to theme setup
add_action( 'init', 'create_posttype' );