<?php
/** 
*
* Template Name: Projects Details
* 
*/
?>
<?php  extract(qode_get_blog_single_params()); ?>
<?php get_header(); ?>
<div class="project_name_body">
    <div class="project_name_title_div">
        <h2 class="project_name_title"><?php echo get_field('project_name'); ?></h2>
    <div>
    <div class="project_main_image_div">
        <img src="<?php echo get_field('project_image'); ?>">
    </div>
</div>
<?php get_footer(); ?>	