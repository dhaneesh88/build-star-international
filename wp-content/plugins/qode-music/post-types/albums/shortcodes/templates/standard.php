<div class="qode-album">
	<div class = "qode-album-image-holder">
		<a href="<?php echo esc_url($album_link); ?>">
			<?php
				echo get_the_post_thumbnail(get_the_ID(),'full');
			?>				
		</a>
	</div>
	<div class="qode-album-text-holder">
		<p class="qode-album-title">
			<a href="<?php echo esc_url($album_link); ?>">
				<?php echo esc_attr(get_the_title()); ?>
			</a>	
		</p>
	</div>
</div>