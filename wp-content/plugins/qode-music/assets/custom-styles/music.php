<?php

if (!function_exists('qode_music_style') && qode_music_theme_installed()) {
    function qode_music_style()
    {

        if (qode_options()->getOptionValue('first_color') !== '') {
            echo qode_dynamic_css('.qode-working-hours-holder .qode-wh-title .qode-wh-title-accent-word,
                                    #ui-datepicker-div .ui-datepicker-current-day:not(.ui-datepicker-today) a, .single-qode-album .qode-album-nav .qode-album-back-btn a:hover, .single-qode-album .qode-album-nav .qode-album-next a:hover, .single-qode-album .qode-album-nav .qode-album-prev a:hover', array(
                'color' => qode_options()->getOptionValue('first_color')
            ));
            echo qode_dynamic_css('#ui-datepicker-div .ui-datepicker-today', array(
                'background-color' => qode_options()->getOptionValue('first_color')
            ));
        }

    }

    add_action('qode_style_dynamic', 'qode_music_style');
}