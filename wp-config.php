<?php
/** Enable W3 Total Cache */
define('WP_CACHE', true); // Added by W3 Total Cache

/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'build_star');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Ew;;4fjMHZ)iVZbd)R>SWp|2uo)U^tdMj22g1<{J)]w*j,xwYKzbtI[5UaL&e4s{');
define('SECURE_AUTH_KEY',  '6a)N Q|2bD{l95_w*iefNI?-17zj{~ja[v13diD2B.E%!|Z>S61Hy$ft(g$I,P+x');
define('LOGGED_IN_KEY',    'Iy M%?Ax$; rx)~++/>Wmu[uej[CA?1[W.Q+S@O ]n?H}hizvT60B~FLiEy>@d#w');
define('NONCE_KEY',        'qA1K}!In1t/:Y(kS~ }xY3s#[]qX?~+r-ylr#9Hk+83~<mF)(_Pn{Oj1&]NFsz<H');
define('AUTH_SALT',        'DYKUZ4oB~6$l^XY5.erlK0&<W_1Q1r6jg+|Ih7JeA+< hF^7!]hS/iYId%O1~-J5');
define('SECURE_AUTH_SALT', 'MdYPE9W2k!c0D?qZ6CyEa^2}5ezfV0urw}D>$8Hmnh(EgUIy[rb(mt]b6&TnDuSy');
define('LOGGED_IN_SALT',   'ARrcEvsfa2/H/SvhZY1|(u+64{JL>N=y<X56m6G7N/Pz~^<zuq(;@ O(t5VauTu,');
define('NONCE_SALT',       '2nTUv8i=Op,S:#O:7=0FcJy$qV]QaO5D0#BcRb~x0..> yH7[4>oR#:iz}H?Ks[9');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'buildstar_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
